# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

Erster Upload des Projekts

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [0.1.2] - 2019-11-21

### Added

- Node-RED Flow File
- Grafana Dashboard für Schulsensore
- Payload Decoder

### Changed

### Deprecated

### Removed

### Fixed

- Waiting for Serial Monitor

### Security



## [0.1.0] - 2019-11-20
### Added

Erster Upload des Projekts

### Changed

### Deprecated

### Removed

### Fixed

### Security
